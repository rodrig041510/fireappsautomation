﻿Feature: Login

Scenario Outline: Login to CashFlow
Given I navigate to the application
When I enter valid Credentials for "<Client>"
Then I should be successfully logged in

Examples: 
| Scenario  | Client           |
| Daily     | Daily Client     |
| Non-Daily | Non Daily Client |