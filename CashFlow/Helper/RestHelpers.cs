﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using CashFlow.Model;

namespace CashFlow.Helper
{
	public class RestHelpers
	{
		private static string BaseUrl;
		public static Response GetResponse(string baseUrl, string method, Request request)
		{
			BaseUrl = baseUrl;
			var restClient = new RestClient(BaseUrl);
			var restRequest = new RestRequest(method, Method.POST);
			restRequest.RequestFormat = DataFormat.Json;

			var jsonRequest = restRequest.JsonSerializer.Serialize(request);

			restRequest.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

			restRequest.AddHeader("Content-type", "application/json; charset=utf-8");

			IRestResponse restResponse = restClient.Execute(restRequest);

			var resp = JsonConvert.DeserializeObject<Response>(restResponse.Content.ToString());

			return resp;
		}
	}
}
