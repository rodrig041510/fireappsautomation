﻿using Framework.Base;
using Framework.Config;
using Framework.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;

namespace DataHubAutomation.Hooks
{
	[Binding]
	public class HookInitialize : TestInitializeHook
	{
		private static bool isInvoked = false;
		[BeforeStep]
		public static void TestStart()
		{
			if (isInvoked) return;
			isInvoked = true;
			var testContext = ScenarioContext.Current.ScenarioContainer.Resolve<TestContext>();

			string testResults = testContext.TestResultsDirectory;
			string server = string.Empty;

			if (string.IsNullOrEmpty(testResults))
				testResults = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\TestResults");
			TakeScreenShot.CurrentDirectory = testResults;
			if (testContext.Properties.Contains("environment"))
				server = testContext.Properties["environment"].ToString();
			else if (string.IsNullOrEmpty(server))
				server = ConfigurationManager.AppSettings["environment"];

			InitializeSettings(server);

			if (testContext.Properties.Contains("browser"))
			{
				var browser = testContext.Properties["browser"].ToString();
				if (!string.IsNullOrEmpty(browser))
					Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), browser);
			}
		}

		[After]
		public static void TestStop()
		{
			if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("API"))
			{
				TestContext testContext1 = ScenarioContext.Current.ScenarioContainer.Resolve<Microsoft.VisualStudio.TestTools.UnitTesting.TestContext>();

				TakeScreenShot.TestTitle = testContext1.TestName;
				if (ScenarioContext.Current.TestError != null)
				{
					if (TakeScreenShot.TakeAndSaveScreenshot())
						testContext1.AddResultFile(TakeScreenShot.FilePath);
					if (Settings.IsCloseBrowser == "Y")
						CloseBrowser();
				}
				else
					CloseBrowser();
			}
		}
	}
}
