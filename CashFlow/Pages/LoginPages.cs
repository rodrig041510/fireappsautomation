﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CashFlow.Pages;
using OpenQA.Selenium;
using Framework.Base;
using Framework.Extensions;
using Framework.Helpers;
using Framework.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CashFlow.Pages
{

	public class LoginPages : BasePage
	{
		private IWebElement UsernameTextBox => DriverContext.Driver.FindElement(By.Name("loginfmt"));

		private IWebElement PwdTextBox => DriverContext.Driver.FindElement(By.Name("passwd"));

		private IWebElement Next => DriverContext.Driver.FindElement(By.XPath("//input[@type='submit']"));

		private IWebElement signIn => DriverContext.Driver.FindElement(By.XPath("//input[@class='btn btn-block btn-primary']"));

		public void IEnterCredentialsForCashFlow(string type)
		{
			Dictionary<string, dynamic> data = JsonDataDriver.GetRecord("Credentials", type);
			UsernameTextBox.Clear();
			UsernameTextBox.SendKeys(data.Get("Username"));
			DriverContext.Driver.PageSync(3);
			Next.Click();
			PwdTextBox.Clear();
			PwdTextBox.SendKeys(data.Get("Password"));
			DriverContext.Driver.PageSync(3);
			signIn.Click();
		}
		public void IAmLoggedIn()
		{
			var ContinueLoggedin = By.XPath("//input[@type='button']");
			DriverContext.Driver.WaitUntilElementExists(ContinueLoggedin, TimeSpan.FromSeconds(1));
			DriverContext.Driver.FindElement(ContinueLoggedin).Click();
			DriverContext.Driver.WaitUntilElementExists(By.XPath("//img[@class='logo']"), TimeSpan.FromSeconds(15));
			var fireAppsHome = By.CssSelector("div.home");
			DriverContext.Driver.IsAnyElementPresent(fireAppsHome, TimeSpan.FromSeconds(1));
			var homeCashFlow = DriverContext.Driver.FindElement(fireAppsHome);
			Assert.IsTrue(homeCashFlow.Displayed, "CashFlow home page did not load!!!!");
		}
	}
}
