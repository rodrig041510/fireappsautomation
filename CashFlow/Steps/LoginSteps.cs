﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Framework.Config;
using Framework.Extensions;
using OpenQA.Selenium;
using Framework.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CashFlow.Pages;
using Framework.Base;

namespace CashFlow.Steps
{
	[Binding]
	public class LoginSteps :BaseStep
	{
		[Given(@"I navigate to the application")]
		public void GivenINavigateToTheApplication()
		{

			NavigateSite();
			
		}
		[When(@"I enter valid Credentials for ""(.*)""")]
		public void WhenIEnterValidCredentialsFor(string type)
		{
			CurrentPage = GetInstance<LoginPages>();
			CurrentPage.As<LoginPages>().IEnterCredentialsForCashFlow(type);
		}

		[Then(@"I should be successfully logged in")]
		public void ThenIShouldBeSuccessfullyLoggedIn()
		{
			CurrentPage = GetInstance<LoginPages>();
			DriverContext.Driver.WaitForPageLoaded();
			CurrentPage.As<LoginPages>().IAmLoggedIn();
		}

	}
}
