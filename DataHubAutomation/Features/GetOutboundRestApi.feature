﻿Feature: Get Outbound RestApi

@API @Domain @Get @Outbound
Scenario Outline: Outbound GET domain files 
Given I have a GET request for "<ClientId>" for Outbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId     |
| Client 1 | DomainClient |

@API @SubDomain @Get @Outbound
Scenario Outline: Outbound GET subdomain files 
Given I have a GET request for "<ClientId>" for Outbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId        |
| Client 2 | SubDomainClient |

@API @Download @Domain @Get @Outbound
Scenario Outline: Outbound GET domain file download by file id
Given I need to select file id from "<ClientId>"
And I have a GET request for "<ClientId>" for download Outbound Api
When I execute the Get request
And I should get a successful response
Then I should have a downloaded file

Examples: 
| Scenario        | ClientId        |
| DomainClient    | DomainClient    |

@API @Download @SubDomain @Get @Outbound
Scenario Outline: Outbound GET Subdomain file download by file id
Given I need to select file id from "<ClientId>"
And I have a GET request for "<ClientId>" for download Outbound Api
When I execute the Get request
And I should get a successful response
Then I should have a downloaded file

Examples: 
| Scenario        | ClientId        |
| SubDomainClient | SubDomainClient |

@API @Domain @Get @Outbound
Scenario Outline: Outbound GET Domain Directory SubDomain file information 
Given I have a GET request for "<ClientId>" for Directory Outbound Api
When I execute the Get request
And I should get a successful response
Then I should get back results

Examples: 
| Scenario | ClientId     |
| Client 1 | DomainClient |