﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataHubAutomation.Model
{
	public class Response
	{
		public string id { get; set; }
		public string clientId { get; set; }
		public string product { get; set; }
		public string domain { get; set; }
		public string name { get; set; }
		public string hash { get; set; }
		public int bytes { get; set; }
		public DateTime lastModified { get; set; }
	}
}
