﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Framework.Base;
using Framework.Extensions;
using Framework.Helpers;
using Framework.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using TechTalk.SpecFlow;
using Newtonsoft.Json;
using DataHubAutomation.Model;
using System.Security.Cryptography;

namespace DataHubAutomation.Pages
{
	public class InOutboundPage : BasePage
	{
		public void GetRequestOutboundDomain(string client)
		{
			string type = "outbound";
			Dictionary<string, dynamic> data = JsonDataDriver.GetRecord("ClientInformation", client);
			string method = string.Format("/api/io/{0}/{1}/{2}/{3}", data.Get("clientId"), data.Get("product"), data.Get("domain"), data.Get("subdomain"));
			ScenarioContext.Current["method"] = method;
			ScenarioContext.Current["X-Api-Key"] = data.Get("x-api-key");
			ScenarioContext.Current["type"] = type;
		}

		public void GetRequestInboundDomain(string client)
		{
			string type = "inbound";
			Dictionary<string, dynamic> data = JsonDataDriver.GetRecord("ClientInformation", client);
			string method = string.Format("/api/io/{0}/{1}/{2}/{3}", data.Get("clientId"), data.Get("product"), data.Get("domain"), data.Get("subdomain"));
			ScenarioContext.Current["method"] = method;
			ScenarioContext.Current["X-Api-Key"] = data.Get("x-api-key");
			ScenarioContext.Current["type"] = type;
		}

		public void GetRequestDownloadOutbound(string client)
		{
			Dictionary<string, dynamic> data = JsonDataDriver.GetRecord("ClientInformation", client);
			string filePath = "/file?id=" + ScenarioContext.Current.Get("FileId");
			if (data.Get("subdomain") == "")
			{
				string method = string.Format("/api/io/{0}/{1}/{2}" + filePath, data.Get("clientId"), data.Get("product"), data.Get("domain"));
				ScenarioContext.Current["method"] = method;
			}
			else
			{
				string method = string.Format("/api/io/{0}/{1}/{2}/{3}" + filePath, data.Get("clientId"), data.Get("product"), data.Get("domain"), data.Get("subdomain"));
				ScenarioContext.Current["method"] = method;
			}
			ScenarioContext.Current["X-Api-Key"] = data.Get("x-api-key");
		}
		public void RandomFileId(IRestResponse response)
		{
			var content = JsonConvert.DeserializeObject<List<Response>>(response.Content.ToString());
			Random rand = new Random();
			var File = content[rand.Next(0,content.Count)];
			ScenarioContext.Current["FileId"] = File.id;
			ScenarioContext.Current["FileName"] = File.name;
		}
	}
}
