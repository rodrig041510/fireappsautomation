﻿using Framework.Config;
using System;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;

namespace Framework.Base
{
    public abstract class BaseStep : Base
    {
        public virtual void NavigateSite(string url = "")
        {
			TestInitializeHook.OpenBrowser(Settings.BrowserType);
            if (!url.Contains("https://") && !url.Contains("http://"))
                url = selectUrl() == null ? Settings.AUT + url : selectUrl() + url;
            DriverContext.Browser.GoToUrl(url);
            Console.WriteLine("Navigated to: " + url);
        }

        public string selectUrl()
        {
            string environnment = searchTagsForEnvironment();
            Settings set = new Settings();

            //set url based on match in the settings
            string newEnvironment = environnment.Replace("ENV", "").ToUpper().Trim(new char[] { '_' });

            return typeof(Settings).GetMethod("get_" + newEnvironment) != null ?
                (string)typeof(Settings).GetMethod("get_" + newEnvironment).Invoke(set, null) :
                 null;
        }

        protected string searchTagsForEnvironment()
        {
            string environnment = "";

            //search tags for content containing the string url
            for (int i = 0; i < FeatureContext.Current.FeatureInfo.Tags.Length; i++)
            {
                if (FeatureContext.Current.FeatureInfo.Tags[i].ToLower().Contains("env"))
                {
                    environnment = FeatureContext.Current.FeatureInfo.Tags[i];
                    break;
                }
            }

            return environnment;
        }

        protected string removeSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }
    }
}