﻿using OpenQA.Selenium;
using Framework.Extensions;
using System;
using Framework.Config;
using System.Threading;

namespace Framework.Base
{
    public class Browser
    {
        private readonly IWebDriver _driver;

        public Browser(IWebDriver driver)
        {
            _driver = driver;
        }

        public BrowserType Type { get; set; }

        public void GoToUrl(string url)
        {
            DriverContext.Driver.Url = url;
        }

        public static void ScrollToBottom()
        {
            for (int i = 0; i < 5; i++)
            {
                long scrollHeight = 0;
                do
                {
                    var js = (IJavaScriptExecutor)DriverContext.Driver;
                    var newScrollHeight = (long)js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight); return document.body.scrollHeight;");

                    if (newScrollHeight == scrollHeight)
                    {
                        break;
                    }
                    else
                    {
                        scrollHeight = newScrollHeight;
                        DriverContext.Driver.WaitForAngular();
                    }
                } while (true);
                Thread.Sleep(TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .5));
            }
        }
    }

    public enum BrowserType
    {
        IE,
        Firefox,
        Chrome,
        Edge,
        RemoteWebDriver,
        ChromeIphone6,
        ChromeHeadless
    }
}
