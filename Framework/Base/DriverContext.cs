﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Framework.Base
{
    public static class DriverContext
    {
        private static IWebDriver _driver;
        private static WebDriverWait _wait;

        public static IWebDriver Driver
        {
            get
            {
                return _driver;
            }
            set
            {
                _driver = value;
            }
        }

        public static WebDriverWait Wait
        {
            get
            {
                return _wait;
            }
            set
            {
                _wait = value;
            }
        }

        public static Browser Browser { get; set; }
    }
}
