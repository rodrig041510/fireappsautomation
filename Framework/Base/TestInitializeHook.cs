﻿using System.Drawing;
using Framework.Config;
using Framework.Helpers;
using System;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Framework.Base
{
    public abstract class TestInitializeHook : Base
    {        
        public static void InitializeSettings(string settingType = "qa")
        {
            //Set all the settings for framework
            ConfigReader.SetFrameworkSettings(settingType);
        }

        public static void InitializeSettings(TestContext testSettings)
        {
            //Set all the settings for framework
            ConfigReader.SetFrameworkSettings(testSettings);
        }

        public static void InitializeData(string filename)
        {
            ExcelHelpers.PopulateInCollection(filename);
        }

        public static void OpenBrowser(BrowserType browserType = BrowserType.Firefox)
        {
            WebDriverFactory.CreateDriver(browserType);

            DriverContext.Browser = new Browser(DriverContext.Driver);
            DriverContext.Wait = new WebDriverWait(DriverContext.Driver, TimeSpan.FromSeconds(5));
            DriverContext.Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(Settings.LongTimeoutSeconds);
            DriverContext.Driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds);
            DriverContext.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Settings.ImplicitlyWaitSeconds);
            if (browserType == BrowserType.ChromeIphone6)
            {
                DriverContext.Driver.Manage().Window.Size = new Size(385, 682);
            }
            else
                DriverContext.Driver.Manage().Window.Maximize();
        }
      
        public virtual void NavigateSite()
        {
            DriverContext.Browser.GoToUrl(Settings.AUT);
        }

        public static void CloseBrowser()
        {
            DriverContext.Driver.Quit();
        }
    }
}

