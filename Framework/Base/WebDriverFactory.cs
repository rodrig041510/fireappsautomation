﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Configuration;

namespace Framework.Base
{
    public class WebDriverFactory
    {
        private static InternetExplorerOptions InternetExplorerProfile
        {
            get
            {
                var options = new InternetExplorerOptions
                {
                    EnsureCleanSession = true,
                    IgnoreZoomLevel = true,
                };
                return options;
            }
        }

        private static ChromeOptions ChromeProfile
        {
            get
            {
                ChromeOptions options = new ChromeOptions();

                options.AddUserProfilePreference("profile.default_content_settings.popups", 0);
                options.AddUserProfilePreference("download.prompt_for_download", false);

                return options;
            }
        }

        private static ChromeOptions ChromeProfileMobile(string device)
        {
            ChromeOptions chromeCapabilities = new ChromeOptions();

            chromeCapabilities.EnableMobileEmulation(device);
            return chromeCapabilities;
        }

        public static ChromeOptions ChromeProfileHeadless
        {
            get
            {
                ChromeOptions option = new ChromeOptions();
                option.AddArgument("--headless");
                return option;
            }
        }

        private static EdgeOptions EdgeProfile
        {
            get
            {
                var options = new EdgeOptions();
                //options.PageLoadStrategy = EdgePageLoadStrategy.Eager;
                return options;
            }
        }

        private static Uri RemoteWebDriverHub
        {
            get
            {
                return new Uri(ConfigurationManager.AppSettings["RemoteWebDriverHub"]);
            }
        }
        public static void CreateDriver(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.IE:
                    DriverContext.Driver = new InternetExplorerDriver(InternetExplorerProfile);
                    break;
                case BrowserType.Firefox:
                    DriverContext.Driver = new FirefoxDriver();
                    break;
                case BrowserType.Chrome:
                    DriverContext.Driver = new ChromeDriver(ChromeProfile);
                    break;
                case BrowserType.ChromeIphone6:
                    DriverContext.Driver = new ChromeDriver(ChromeProfileMobile("iPhone 6"));
                    break;
                case BrowserType.ChromeHeadless:
                    DriverContext.Driver = new ChromeDriver(ChromeProfileHeadless);
                    break;
                case BrowserType.Edge:
                    DriverContext.Driver = new EdgeDriver(EdgeProfile);
                    break;
                case BrowserType.RemoteWebDriver:
                    var options = ChromeProfile;
                    var desiredCapability = options.ToCapabilities();
                    DriverContext.Driver = new RemoteWebDriver(RemoteWebDriverHub, desiredCapability);
                    break;
                default:
                    throw new NotSupportedException(
                        string.Format("Driver {0} is not supported", browserType));
            }
        }
    }
}
