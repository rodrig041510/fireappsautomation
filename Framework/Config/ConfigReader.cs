﻿using System;
using System.Web;
using Framework.Base;
using Framework.ConfigElement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Framework.Config
{
    public class ConfigReader
    {

        public static void SetFrameworkSettings(string settingType)
        {
            //Set XML Details in the property to be used across framework
            Settings.AUT = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(FireTestConfiguration.Settings.TestSettings[settingType].AUT));
            Settings.TestType = string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].TestType) ?
                "UI" : FireTestConfiguration.Settings.TestSettings[settingType].TestType;
            Settings.Environment = settingType;
            Settings.IsFailConsoleErrors = string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].IsFailConsoleErrors) ?
                "N" : FireTestConfiguration.Settings.TestSettings[settingType].IsFailConsoleErrors;
            Settings.IsCloseBrowser = string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].IsCloseBrowser) ?
                "Y" : FireTestConfiguration.Settings.TestSettings[settingType].IsCloseBrowser;
            Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), FireTestConfiguration.Settings.TestSettings[settingType].Browser);
            Settings.LongTimeoutSeconds = Convert.ToInt32(string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].LongTimeoutSeconds) ?
                "60" : FireTestConfiguration.Settings.TestSettings[settingType].LongTimeoutSeconds);
            Settings.MediumTimeoutSeconds = Convert.ToInt32(string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].MediumTimeoutSeconds) ?
                "10" : FireTestConfiguration.Settings.TestSettings[settingType].MediumTimeoutSeconds);
            Settings.ShortTimeoutSeconds = Convert.ToInt32(string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].ShortTimeoutSeconds) ?
                "3" : FireTestConfiguration.Settings.TestSettings[settingType].ShortTimeoutSeconds);
            Settings.ImplicitlyWaitSeconds = Convert.ToInt32(string.IsNullOrEmpty(FireTestConfiguration.Settings.TestSettings[settingType].ImplicitlyWaitSeconds) ?
                "15" : FireTestConfiguration.Settings.TestSettings[settingType].ImplicitlyWaitSeconds);
        }

        public static void SetFrameworkSettings(TestContext setting)
        {
            Settings.AUT = setting.Properties.Contains("environment") ? setting.Properties[setting.Properties["environment"].ToString().ToLower()].ToString() : null;
            Settings.TestType = setting.Properties.Contains("TestType") ? setting.Properties["TestType"].ToString() : "";
            Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), setting.Properties["browser"].ToString());
            Settings.IsCloseBrowser = setting.Properties.Contains("isCloseBrowser") ? setting.Properties["isCloseBrowser"].ToString() : "Y";
            Settings.LongTimeoutSeconds = setting.Properties.Contains("longTimeoutSeconds") ? Convert.ToInt16(setting.Properties["longTimeoutSeconds"].ToString()) : 60;
            Settings.MediumTimeoutSeconds = setting.Properties.Contains("mediumTimeoutSeconds") ? Convert.ToInt16(setting.Properties["mediumTimeoutSeconds"].ToString()) : 10;
            Settings.ShortTimeoutSeconds = setting.Properties.Contains("shortTimeoutSeconds") ? Convert.ToInt16(setting.Properties["shortTimeoutSeconds"].ToString()) : 3;
            Settings.ImplicitlyWaitSeconds = setting.Properties.Contains("implicitlyWaitSeconds") ? Convert.ToInt16(setting.Properties["implicitlyWaitSeconds"].ToString()) : 15;
            Settings.PaymentKey = setting.Properties.Contains("paymentKey") ? setting.Properties["paymentKey"].ToString() : null;
            if (setting.Properties.Contains("azure"))
            {
                if (setting.Properties["azure"].ToString().ToLower() == "qa")
                {
                    Settings.AzureConnectionString = setting.Properties.Contains("azureQAConnection") ? setting.Properties["azureQAConnection"].ToString() : null;
                    Settings.AzureTable = setting.Properties.Contains("azureQATable") ? setting.Properties["azureQATable"].ToString() : null;
                    Settings.SqlEndPoint = setting.Properties.Contains("sqlQAEndPoint") ? setting.Properties["sqlQAEndPoint"].ToString() : null;
                    Settings.SqlAccountKey = setting.Properties.Contains("sqlQAAccountKey") ? setting.Properties["sqlQAAccountKey"].ToString() : null;
                }
                else
                {
                    Settings.AzureConnectionString = setting.Properties.Contains("azureProdConnection") ? setting.Properties["azureProdConnection"].ToString() : null;
                    Settings.AzureTable = setting.Properties.Contains("azureProdTable") ? setting.Properties["azureProdTable"].ToString() : null;
                    Settings.SqlEndPoint = setting.Properties.Contains("sqlProdEndPoint") ? setting.Properties["sqlProdEndPoint"].ToString() : null;
                    Settings.SqlAccountKey = setting.Properties.Contains("sqlProdAccountKey") ? setting.Properties["sqlProdAccountKey"].ToString() : null;
                }
            }
            //Settings.JWTEndpoint = setting.Properties.Contains("jwtEndPoint") ? setting.Properties[setting.Properties["jwtEndPoint"].ToString()].ToString(): null;
            Settings.JWT = setting.Properties.Contains("jwtEndPoint") ? setting.Properties["jwtEndPoint"].ToString() : null;
            Settings.APP_ID = setting.Properties.Contains("jwtAppID") ? setting.Properties["jwtAppID"].ToString() : null;
            Settings.APP_KEY = setting.Properties.Contains("jwtAppKey") ? setting.Properties["jwtAppKey"].ToString() : null;
            Settings.DEV_IDENTIFIER = setting.Properties.Contains("jwtProcessName") ? setting.Properties["jwtProcessName"].ToString() : null;
        }
    }
}
