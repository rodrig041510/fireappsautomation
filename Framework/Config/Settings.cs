﻿using Framework.Base;
using System.Data.SqlClient;

namespace Framework.Config
{
    public class Settings
    {
        public static string IsReporting { get; set; }

        public static string TestType { get; set; }

        public static string AUT { get; set; }

        public static string JWT { get; set; }

        public static string APP_ID { get; set; }

        public static string APP_KEY { get; set; }

        public static string Icicle_BaseUrl { get; set; }

        public static string DEV_IDENTIFIER { get; set; }

        public static string QA { get; set; }

        public static string STAGING { get; set; }

        public static string PARTNER { get; set; }

        public static string BuildName { get; set; }

        public static BrowserType BrowserType { get; set; }

        public static SqlConnection ApplicationCon { get; set; }

        public static string AppConnectionString { get; set; }

        public static string IsBooking { get; set; }

        public static string IsFailConsoleErrors { get; set; }

        public static string CreditCard { get; set; }

        public static string IsCloseBrowser { get; set; }

        public static int LongTimeoutSeconds { get; set; }

        public static int MediumTimeoutSeconds { get; set; }

        public static int ShortTimeoutSeconds { get; set; }

        public static int ImplicitlyWaitSeconds { get; set; }

        private static bool _fileCreated = false;

        public static string AzureConnectionString { get; set; }

        public static string AzureStorageAccount { get; set; }

        public static string AzureAccountKey { get; set; }

        public static string AzureTable { get; set; }

        public static string SqlEndPoint { get; set; }

        public static string SqlAccountKey { get; set; }
        
        public static string PaymentKey { get; set; }

        public static string JWTEndpoint { get; set; }

        public static bool FileCreated
        {
            get
            {
                return _fileCreated;
            }
            set
            {
                _fileCreated = value;
            }
        }

        public static string Environment { get; set; }
    }
}
