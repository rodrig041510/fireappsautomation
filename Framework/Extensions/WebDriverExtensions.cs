﻿using Framework.Base;
using Framework.Config;
using Framework.Helpers;
using Framework.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace Framework.Extensions
{
    /// <summary>
    /// Extension methods for IWebDriver
    /// </summary>
    public static class WebDriverExtensions
    {
        /// <summary>
        /// Handler for simple use JavaScriptAlert.
        /// </summary>
        /// <example>Sample confirmation for java script alert: <code>
        /// this.Driver.JavaScriptAlert().ConfirmJavaScriptAlert();
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <returns>JavaScriptAlert Handle</returns>
        public static JavaScriptAlert JavaScriptAlert(this IWebDriver webDriver)
        {
            return new JavaScriptAlert(webDriver);
        }

        /// <summary>
        /// Navigates to.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="url">The URL.</param>
        public static void NavigateTo(this IWebDriver webDriver, Uri url)
        {
            webDriver.Navigate().GoToUrl(url);

            ApproveCertificateForInternetExplorer(webDriver);
            ApproveCertificateForInternetExplorer(webDriver);
        }

        /// <summary>
        /// Waits for all ajax actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        public static void WaitForAjax(this IWebDriver webDriver)
        {
            WaitForAjax(webDriver, TimeSpan.FromSeconds(Settings.MediumTimeoutSeconds));
        }

        /// <summary>
        /// Waits for all ajax actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="timeout">The timeout.</param>
        public static void WaitForAjax(this IWebDriver webDriver, TimeSpan timeout)
        {
            try
            {
                new WebDriverWait(webDriver, timeout).Until(
                    driver =>
                    {
                        var javaScriptExecutor = driver as IJavaScriptExecutor;
                        return javaScriptExecutor != null
                               && (bool)javaScriptExecutor.ExecuteScript("return jQuery.active == 0");
                    });
            }
            catch (InvalidOperationException)
            {
            }
        }

        /// <summary>
        /// Wait for element to be displayed for specified time
        /// </summary>
        /// <example>Example code to wait for login Button: <code>
        /// this.Driver.IsElementPresent(this.loginButton, BaseConfiguration.ShortTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        /// <param name="customTimeout">The timeout.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsElementPresent(this IWebDriver webDriver, By locator, TimeSpan customTimeout)
        {
            try
            {
                webDriver.GetElement(locator, customTimeout, e => e.Displayed);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// Wait for element to be displayed for specified time
        /// </summary>
        /// <example>Example code to wait for login Button: <code>
        /// this.Driver.IsElementPresent(this.loginButton, BaseConfiguration.ShortTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsElementPresent(this IWebDriver webDriver, By locator)
        {
            try
            {
                webDriver.GetElement(locator, TimeSpan.FromSeconds(Settings.MediumTimeoutSeconds), e => e.Displayed);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether [is page title] equals [the specified page title].
        /// </summary>
        /// <example>Sample code to check page title: <code>
        /// this.Driver.IsPageTitle(expectedPageTitle, Settings.MediumTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="pageTitle">The page title.</param>
        /// <returns>
        /// Returns title of page
        /// </returns>
        public static bool IsPageTitle(this IWebDriver webDriver, string pageTitle)
        {
            return IsPageTitle(webDriver, pageTitle, TimeSpan.FromSeconds(Settings.MediumTimeoutSeconds));
        }

        /// <summary>
        /// Determines whether [is page title] equals [the specified page title].
        /// </summary>
        /// <example>Sample code to check page title: <code>
        /// this.Driver.IsPageTitle(expectedPageTitle, Settings.MediumTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="pageTitle">The page title.</param>
        /// <param name="timeout">The timeout.</param>
        /// <returns>
        /// Returns title of page
        /// </returns>
        public static bool IsPageTitle(this IWebDriver webDriver, string pageTitle, TimeSpan timeout)
        {
            var wait = new WebDriverWait(webDriver, timeout);

            try
            {
                wait.Until(d => d.Title.ToLower(CultureInfo.CurrentCulture) == pageTitle.ToLower(CultureInfo.CurrentCulture));
            }
            catch (WebDriverTimeoutException)
            {
                Console.Write(string.Format(CultureInfo.CurrentCulture, "Actual page title is {0};", webDriver.Title));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Waits the until element is no longer found.
        /// </summary>
        /// <example>Sample code to check page title: <code>
        /// this.Driver.WaitUntilElementIsNoLongerFound(dissapearingInfo, BaseConfiguration.ShortTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        /// <param name="timeout">The timeout.</param>
        public static void WaitUntilElementIsNoLongerFound(this IWebDriver webDriver, By locator, TimeSpan timeout)
        {
            var wait = new WebDriverWait(webDriver, timeout);
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException), typeof(NoSuchElementException));
            wait.Until(driver => webDriver.GetElements(locator).Count == 0);
        }

        /// <summary>
        /// Waits the until element exists.
        /// </summary>
        /// this.Driver.WaitUntilElementExists(locator, BaseConfiguration.ShortTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        /// <param name="timeout">The timeout.</param>
        public static void WaitUntilElementExists(this IWebDriver webDriver, By locator, TimeSpan timeout)
        {
            var wait = new WebDriverWait(webDriver, timeout);
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException), typeof(NoSuchElementException));
            wait.Until(driver => webDriver.GetElements(locator).Count > 0);
        }

        /// <summary>
        /// Switch to existing window using url.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="url">The url.</param>
        /// <param name="timeout">The timeout.</param>
        public static void SwitchToWindowUsingUrl(this IWebDriver webDriver, Uri url, TimeSpan timeout)
        {
            var wait = new WebDriverWait(webDriver, timeout);
            wait.Until(
                driver =>
                {
                    foreach (var handle in webDriver.WindowHandles)
                    {
                        webDriver.SwitchTo().Window(handle);
                        if (driver.Url.Equals(url.ToString()))
                        {
                            return true;
                        }
                    }

                    return false;
                });
        }

        /// <summary>
        /// The scroll into middle.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        public static void ScrollIntoMiddle(this IWebDriver webDriver, By locator)
        {
            var js = (IJavaScriptExecutor)webDriver;
            var element = webDriver.ToDriver().GetElement(locator);

            if (webDriver != null)
            {
                int height = webDriver.Manage().Window.Size.Height;

                var hoverItem = (ILocatable)element;
                var locationY = hoverItem.LocationOnScreenOnceScrolledIntoView.Y;
                js.ExecuteScript(string.Format(CultureInfo.InvariantCulture, "javascript:window.scrollBy(0,{0})", locationY - (height / 2)));
            }
        }
        /// <summary>
        /// The scroll to the end.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        public static void ScrollToTheEnd(this IWebDriver webDriver, By locator)
        {
            var js = (IJavaScriptExecutor)webDriver;
            var element = webDriver.ToDriver().GetElement(locator);

            if (webDriver != null)
            {
                int height = webDriver.Manage().Window.Size.Height;

                var hoverItem = (ILocatable)element;
                var locationY = hoverItem.LocationOnScreenOnceScrolledIntoView.Y;
                js.ExecuteScript(string.Format(CultureInfo.InvariantCulture, "javascript:window.scrollBy(0,{0})", locationY - (height)));
            }
        }
        /// <summary>
        /// Selenium Actions.
        /// </summary>
        /// <example>Simple use of Actions: <code>
        /// this.Driver.Actions().SendKeys(Keys.Return).Perform();
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <returns>Return new Action handle</returns>
        public static Actions Actions(this IWebDriver webDriver)
        {
            return new Actions(webDriver);
        }

        /// <summary>Checks that page source contains text for specified time.</summary>
        /// <param name="webDriver">The web webDriver.</param>
        /// <param name="text">The text.</param>
        /// <param name="timeout">The timeout.</param>
        /// <param name="isCaseSensitive">True if this object is case sensitive.</param>
        /// <returns>true if it succeeds, false if it fails.</returns>
        public static bool PageSourceContainsCase(this IWebDriver webDriver, string text, TimeSpan timeout, bool isCaseSensitive)
        {
            Func<IWebDriver, bool> condition;

            if (isCaseSensitive)
            {
                condition = drv => drv.PageSource.Contains(text);
            }
            else
            {
                condition = drv => drv.PageSource.ToUpperInvariant().Contains(text.ToUpperInvariant());
            }

            if (timeout.Seconds > 0)
            {
                var wait = new WebDriverWait(webDriver, timeout);
                return wait.Until(condition);
            }

            return condition.Invoke(webDriver);
        }

        /// <summary>Easy use for java scripts.</summary>
        /// <example>Sample use of java scripts: <code>
        /// this.Driver.JavaScripts().ExecuteScript("return document.getElementById("demo").innerHTML");
        /// </code></example>
        /// <param name="webDriver">The webDriver to act on.</param>
        /// <returns>An IJavaScriptExecutor Handle.</returns>
        public static IJavaScriptExecutor JavaScripts(this IWebDriver webDriver)
        {
            return (IJavaScriptExecutor)webDriver;
        }

        /// <summary>Easy use for java scripts.</summary>
        /// <example>Sample use of java scripts: <code>
        /// this.Driver.ExecuteJs("return document.getElementById("demo").innerHTML");
        /// </code></example>
        /// <param name="webDriver">The webDriver to act on.</param>
        /// <returns>An IJavaScriptExecutor Handle.</returns>
        public static object ExecuteJs(this IWebDriver driver, string script)
        {
            return ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript(script);
        }

        /// <summary>
        /// Waits for all angular actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        public static void WaitForAngular(this IWebDriver webDriver)
        {
            WaitForAngular(webDriver, TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds));
        }

        /// <summary>
        /// Waits for all angular actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="timeout">The timeout.</param>
        public static void WaitForAngular(this IWebDriver webDriver, TimeSpan timeout)
        {
            try
            {
                new WebDriverWait(webDriver, timeout).Until(
                    driver =>
                    {
                        var javaScriptExecutor = driver as IJavaScriptExecutor;
                        return javaScriptExecutor != null
                               &&
                               (bool)javaScriptExecutor.ExecuteScript("return window.angular != undefined && window.angular.element(document.body).injector().get('$http').pendingRequests.length == 0");
                    });
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Waits for Page actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        public static void WaitForPageLoaded(this IWebDriver driver)
        {
            WaitForPageLoaded(driver, TimeSpan.FromSeconds(Settings.LongTimeoutSeconds));
        }

        /// <summary>
        /// Waits for Page actions to be completed.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="timeout">The timeout.</param>
        public static void WaitForPageLoaded(this IWebDriver driver, TimeSpan timeout)
        {
            driver.WaitForCondition(dri =>
            {
                string state = dri.ExecuteJs("return document.readyState").ToString();
                return state == "complete";
            }, timeout);
        }

        /// <summary>
        /// return all the elements that are displayed and enabled
        /// </summary>
        /// <example>Example code to return if any button is displayed and enabled: <code>
        /// this.Driver.IsAnyElementPresent(this.loginButton, BaseConfiguration.ShortTimeout);
        /// </code></example>
        /// <param name="webDriver">The web driver.</param>
        /// <param name="locator">The locator.</param>
        /// <param name="customTimeout">The timeout.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsAnyElementPresent(this IWebDriver webDriver, By locator, TimeSpan customTimeout)
        {
                var elements = webDriver.GetElements(locator, customTimeout);
                return (elements.Count > 0);
        }

        /// <summary>
        /// Approves the trust certificate for internet explorer.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        private static void ApproveCertificateForInternetExplorer(this IWebDriver webDriver)
        {
            if (Settings.BrowserType == BrowserType.IE && webDriver.Title.Contains("Certificate"))
            {
                webDriver.FindElement(By.Id("overridelink")).JavaScriptClick();
            }
        }

        public static void WaitForCondition<T>(this T obj, Func<T, bool> condition, TimeSpan timeout)
        {
            Func<T, bool> execute =
                (arg) =>
                {
                    try
                    {
                        return condition(arg);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                };

            var timer = Stopwatch.StartNew();
            while (timer.ElapsedMilliseconds < timeout.TotalMilliseconds)
            {
                if (execute(obj))
                {
                    break;
                }
                Thread.Sleep(1); // so processor can rest for a while
            }
        }

        public static IWebElement WaitForElementToBeCLickable(this IWebDriver driver, By by, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(by));

            return element;
        }

        public static IWebElement WaitForElementToBeCLickable(this IWebDriver driver, IWebElement thisElement, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(thisElement));

            return element;
        }
        public static void DragAndDrop(this IWebDriver driver, IWebElement from, IWebElement to)

        {

            (new Actions(driver)).DragAndDrop(from, to).Perform();

        }

        public static void PageSync(this IWebDriver driver, int waitTime = 15)
        {
            By spinner1 = By.XPath("//div[@class='spinner']");
            DriverContext.Wait.Timeout = TimeSpan.FromSeconds(waitTime);

            try
            {
                DriverContext.Wait.Until(ExpectedConditions.StalenessOf(DriverContext.Wait.Until(ExpectedConditions.ElementExists(spinner1))));
                DriverContext.Wait.Timeout = TimeSpan.FromSeconds(5);
            }
            catch { }
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }
    }
}

