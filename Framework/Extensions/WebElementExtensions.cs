﻿using Framework.Base;
using Framework.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace Framework.Extensions
{
    /// <summary>
    /// Extension methods for IWebElement
    /// </summary>
    public static class WebElementExtensions
    {
        /// <summary>
        /// Verify if actual element text equals to expected.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        /// <param name="text">The text.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsElementTextEqualsToExpected(this IWebElement webElement, string text)
        {
            return webElement.Text.Equals(text);
        }

        public static void Click(this IWebElement webElement)
        {
            webElement.Click();
        }

        /// <summary>
        /// Set element attribute using java script.
        /// </summary>
        /// <example>Sample code to check page title: <code>
        /// this.Driver.SetAttribute(this.username, "attr", "10");
        /// </code></example>
        /// <param name="webElement">The web element.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="attributeValue">The attribute value.</param>
        /// <exception cref="System.ArgumentException">Element must wrap a web driver
        /// or
        /// Element must wrap a web driver that supports java script execution</exception>
        public static void SetAttribute(this IWebElement webElement, string attribute, string attributeValue)
        {
            var javascript = webElement.ToDriver() as IJavaScriptExecutor;
            if (javascript == null)
            {
                throw new ArgumentException("Element must wrap a web driver that supports javascript execution");
            }

            javascript.ExecuteScript(
                "arguments[0].setAttribute(arguments[1], arguments[2])",
                webElement,
                attribute,
                attributeValue);
        }

        /// <summary>
        /// Set element value using java script.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        /// <param name="driver">The web driver.</param>
        /// <param name="value">The value to set on webElement.</param>
        /// For elements created by Page Factory the driver has to be passed
        public static void SetValue(this IWebElement webElement, IWebDriver driver, string value)
        {
            var javascript = driver as IJavaScriptExecutor;

            javascript.ExecuteScript("arguments[0].value='" + value + "'", webElement);
        }

        /// <summary>
        /// Set element value using java script.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        /// <param name="driver">The web driver.</param>
        /// <param name="value">The value to set on webElement.</param>
        /// <exception cref = "System.ArgumentException" > Element must wrap a web driver
        /// or
        /// Element must wrap a web driver that supports java script execution</exception> 
        public static void SetValue(this IWebElement webElement, string value)
        {
            var javascript = webElement.ToDriver() as IJavaScriptExecutor;
            if (javascript == null)
            {
                throw new ArgumentException("Element must wrap a web driver that supports javascript execution");
            }

            javascript.ExecuteScript("arguments[0].value='" + value + "'", webElement);
        }

        /// <summary>
        /// Click on element using java script.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        /// <param name="driver">The web driver.</param>
        ///  /// For elements created by Page Factory the driver has to be passed
        public static void JavaScriptClick(this IWebElement webElement, IWebDriver driver)
        {
            var javascript = driver as IJavaScriptExecutor;
            javascript.ExecuteScript("arguments[0].click();", webElement);
        }

        /// <summary>
        /// Click on element using java script.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        public static void JavaScriptClick(this IWebElement webElement)
        {
            var javascript = webElement.ToDriver() as IJavaScriptExecutor;
            if (javascript == null)
            {
                throw new ArgumentException("Element must wrap a web driver that supports javascript execution");
            }

            javascript.ExecuteScript("arguments[0].click();", webElement);
        }

        /// <summary>
        /// Returns the textual content of the specified node, and all its descendants regardless element is visible or not.
        /// </summary>
        /// <param name="webElement">The web element</param>
        /// <returns>The attribute</returns>
        /// <exception cref="ArgumentException">Element must wrap a web driver
        /// or
        /// Element must wrap a web driver that supports java script execution</exception>
        public static string GetTextContent(this IWebElement webElement)
        {
            var javascript = webElement.ToDriver() as IJavaScriptExecutor;
            if (javascript == null)
            {
                throw new ArgumentException("Element must wrap a web driver that supports javascript execution");
            }

            var textContent = (string)javascript.ExecuteScript("return arguments[0].textContent", webElement);
            return textContent;
        }

        public static string GetSelectedDropDown(this IWebElement element)
        {
            SelectElement ddl = new SelectElement(element);
            return ddl.AllSelectedOptions.First().ToString();
        }

        public static IList<IWebElement> GetSelectedListOptions(this IWebElement element)
        {
            SelectElement ddl = new SelectElement(element);
            return ddl.AllSelectedOptions;
        }

        public static string GetLinkText(this IWebElement element)
        {
            return element.Text;
        }

        public static void SelectDropDownList(this IWebElement element, string value)
        {
            SelectElement ddl = new SelectElement(element);
            ddl.SelectByText(value);
        }

        public static void Hover(this IWebElement element)
        {
            Actions actions = new Actions(DriverContext.Driver);
            actions.MoveToElement(element).Perform();
        }

        /// <summary>
        /// Throw Exception if element is not displayed
        /// </summary>
        /// <param name="webElement">The web element</param>
        /// <exception cref="Exception">Element Not Present exception</exception>
        public static void AssertElementPresent(this IWebElement webElement)
        {
            if (!IsElementPresent(webElement))
                throw new Exception(string.Format("Element Not Present exception"));
        }

        /// <summary>
        /// Return true of false if a element is displayed
        /// </summary>
        /// <param name="webElement">The web element</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsElementPresent(this IWebElement webElement)
        {
            try
            {
                DriverContext.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                bool ele = webElement.Displayed;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                DriverContext.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Settings.ImplicitlyWaitSeconds);
            }
        }

        /// <summary>
        /// Highlight a element for easy identification
        /// </summary>
        /// <param name="webElement">The web element</param>
        public static void Highlight(this IWebElement webElement, int duration =2)
        {
            var originalStyle = webElement.GetAttribute("style");
            var jsDriver = (IJavaScriptExecutor)DriverContext.Driver;

            var script = @"arguments[0].style.cssText = ""border-width: 2px; border-style: dashed; border-color: red"";";
            jsDriver.ExecuteScript(script, new object[] { webElement });
            if (duration <= 0) return;
            Thread.Sleep(TimeSpan.FromSeconds(duration));
            script = @"arguments[0].setAttribute(arguments[1], arguments[2])";
            jsDriver.ExecuteScript(script, webElement, "style", originalStyle);
        }

        public static void ScrollIntoView(this IWebElement webElement)
        {
            var jsDriver = (IJavaScriptExecutor)DriverContext.Driver;

            var script = @"arguments[0].scrollIntoView(true);";
            jsDriver.ExecuteScript(script, new object[] { webElement });
        }

        public static void ScrollToView(this IWebElement webElement)
        {
            if (webElement.Location.Y > 200)
            {
                ScrollTo(0, webElement.Location.Y - 200); // Make sure element is in the view but below the top navigation pane
            }
        }

        private static void ScrollTo(int xPosition = 0, int yPosition = 0)
        {
            var JavaScriptExecutor = (IJavaScriptExecutor)DriverContext.Driver;

            var js = String.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
            JavaScriptExecutor.ExecuteScript(js);
        }


        /// <summary>
        /// Check if image is broken
        /// </summary>
        /// <param name="webElement">The web element</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public static bool IsImageBroken(this IWebElement webElement)
        {
            var jsDriver = (IJavaScriptExecutor)DriverContext.Driver;

            var script = @"return arguments[0].complete && typeof arguments[0].naturalWidth != 'undefined' && arguments[0].naturalWidth > 0;";
            var isFine = jsDriver.ExecuteScript(script, new object[] { webElement });
            return !(bool)isFine;
        }

        /// <summary>
        /// Clean the field before typing text
        /// </summary>
        /// <param name="webElement">The web element</param>
        /// <param name="text">The text.</param>
        /// <param name="clearFirst">flag to enable clearing</param>
        public static void SendKeys(this IWebElement webElement, string text, bool clearFirst)
        {
            if (clearFirst) webElement.Clear();
            webElement.SendKeys(text);
        }

        public static void SetSlider(this IWebElement webElement, double value, double maxValue = 0)
        {
            double minValue = Convert.ToDouble(webElement.GetAttribute("max"));
            maxValue = maxValue == 0 ? Convert.ToDouble(webElement.GetAttribute("min")) : maxValue;
            int sliderH = webElement.Size.Height;
            int sliderW = webElement.Size.Width;
            Actions action = new Actions(DriverContext.Driver);
            action.MoveToElement(webElement, (int)(value * sliderW / (maxValue - minValue)), sliderH / 2).Click().Build().Perform();
        }

        /// <summary>
        /// Removes Special Characters
        /// </summary>
        /// <param name="webElement"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveSpecialCharacters(this IWebElement webElement)
        {
            String str = webElement.GetTextContent();
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// Changes First Character to upper case only
        /// </summary>
        /// <param name="webElement"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToTitleCase(this IWebElement webElement)
        {
            String str = webElement.GetTextContent();
            return Regex.Replace(str, "^[a-z]", m => m.Value.ToUpper());
        }

        /// <summary>
        /// Removes All whitespace in a String
        /// </summary>
        /// <param name="webElement"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveAllWhiteSpaces(this IWebElement webElement)
        {
            var str = webElement.GetTextContent();
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
            
        }
        /// <summary>
        /// Click an element from an Angular Drop Down list by Name
        /// </summary>
        /// <param name="webElement should be of type class='md-select-menu-container.md-active .md-text'"></param>
        /// <param name="choice">Text to match on the click</param>
        /// <param name="selector">By Selector that opens the drop down list</param>
        public static void SelectAngularDropListByName(this IWebElement webElement, string choice, By selector)
        {
            webElement.JavaScriptClick();
            Thread.Sleep(TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));
            var pick = Regex.Replace(choice, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);

            var elements = DriverContext.Driver.GetElements(selector);
            foreach (var element in elements)
            {
                string text = element.RemoveSpecialCharacters();
                if (text.IndexOf(pick, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    element.ScrollIntoView();
                    element.JavaScriptClick();
                    element.WaitForCondition(e => e.Displayed == false, TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));
                    return;
                }
            }
            throw new Exception("Element not found on Drop Down List: " + choice);
        }

        /// <summary>
        /// Click an element from an Angular Drop Down list by Index
        /// </summary>
        /// <param name="webElement should be of type class='md-select-menu-container.md-active .md-text'"></param>
        /// <param name="index">Index starts from 1, if Index is 0 will select the last item </param>
        /// <param name="selector">By Selector that opens the drop down list</param>
        public static void SelectAngularDropListByIndex(this IWebElement webElement, int index, By selector)
        {
            webElement.JavaScriptClick();
            Thread.Sleep(TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));

            var elements = DriverContext.Driver.GetElements(selector);

            if (index < 0)
                index = elements.Count - 1;

            if (elements.Count - 1 < index)
                throw new IndexOutOfRangeException("Index error: " + index + " and Elements on list: " + elements.Count);

            var element = elements[index];
            element.ScrollIntoView();
            element.JavaScriptClick();
            element.WaitForCondition(e => e.Displayed == false, TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));
        }

        /// <summary>
        /// Click an element from an Angular Drop Down list randomly
        /// </summary>
        /// <param name="webElement should be of type class='md-select-menu-container.md-active .md-text'"></param>
        /// <param name="selector">By Selector that opens the drop down list</param>
        public static void PickRandomAngularDropListElement(this IWebElement webElement, By selector)
        {
            Random random = new Random();
            webElement.ScrollIntoView();
            webElement.JavaScriptClick();
            Thread.Sleep(TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));

            var elements = DriverContext.Driver.GetElements(selector);
            var element = elements[random.Next(0, elements.Count)];
            element.ScrollIntoView();
            element.JavaScriptClick();
            element.WaitForCondition(e => e.Displayed == false, TimeSpan.FromSeconds(Settings.ShortTimeoutSeconds * .3));
        }

        /// <summary>
        /// Scroll an Angular container
        /// </summary>
        /// <param name="scrollContainer should be of type class='md-virtual-repeat-scroller'"></param>
        /// <param name="up"></param>
        /// <returns>Scroll - if the scrollContainer.scrollTop value isn't changed, return false
        ///                   This means the scroll didn't occur - there may be no more data to load
        /// </returns>
        public static bool ScrollAngularContainer(this IWebElement scrollContainer, bool up = false)
        {
            string scrollScript = "function scrollDown(container) { " +
                                      "var initHeight = container.scrollHeight; " +
                                      "var initTop = container.scrollTop; " +
                                      "container.scrollTop = initHeight; " +
                                      "var scrolled = initTop != container.scrollTop; " +
                                      "return scrolled; }" +
                                  "function scrollUp(container) {" +
                                      "var initTop = container.scrollTop;" +
                                      "var initHeight = container.scrollHeight;" +
                                      "container.scrollTop = initTop - initHeight;" +
                                      "var scrolled = initTop != container.scrollTop;" +
                                      "return scrolled; }";

            if (up)
            {

                scrollScript += $" return scrollUp(arguments[0])";
            }
            else
            {
                scrollScript += $" return scrollDown(arguments[0])";
            }

            IJavaScriptExecutor executor = (IJavaScriptExecutor)DriverContext.Driver;

            return (bool)executor.ExecuteScript(scrollScript, scrollContainer);
        }

    }
}