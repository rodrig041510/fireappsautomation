﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Framework.Helpers
{
    public class AzureStorageService
    {
        private CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;
        private CloudTable table { get; set; }

        public AzureStorageService(string storageConnectionString, string setTable)
        {
            try
            {
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
                tableClient = storageAccount.CreateCloudTableClient();
                table = tableClient.GetTableReference(setTable);
            }
            catch (Exception ex)
            {
                Assert.Fail("Unable to connect to Azure Storage Service. Exception Message: {0}", ex.Message);
            }
        }

        public IEnumerable<DynamicTableEntity> query(string filter)
        {
            IEnumerable<DynamicTableEntity> queryResults;
            try
            {
                TableQuery<DynamicTableEntity> query = new TableQuery<DynamicTableEntity>().Where(filter);
                queryResults = table.ExecuteQuery(query);
                return queryResults;
            }
            catch (Exception ex)
            {
                Assert.Fail("An issue occurred while querying the Azure Storage Account. Exception Message: {0}", ex.Message);
                return null;
            }
        }
    }
}
