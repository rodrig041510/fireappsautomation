﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Framework.Helpers
{
    public class JsonDataDriver
    {
        public static Dictionary<string, dynamic> GetRecord(string jsonFile, string key)
        {
            var fileDirectory = Environment.CurrentDirectory.ToString() + @"\Data\" + jsonFile + @".json";
            try
            {
                using (StreamReader reader = new StreamReader(fileDirectory))
                {
                    string jsonContents = reader.ReadToEnd();
                    JObject items = JObject.Parse(jsonContents);
                    Dictionary<string, dynamic> data;
                    if (items[key] == null)
                    {
                        data = new Dictionary<string, dynamic>();
                        data.Add("1", key);
                        return data; 
                    }
                    else
                    {
                        data = items[key].ToObject<Dictionary<string, dynamic>>();
                        return checkForKeys(data);
                    }
                    
                }
            }
            catch (FileNotFoundException)
            {
                Assert.Fail("The json file could not be found: " + jsonFile + ".json");
            }
            catch(Exception e)
            {
                Assert.Fail(e.Message);
            }
            return null;
        }


        private static Dictionary<string, dynamic> checkForKeys(Dictionary<string, dynamic> dict)
        {
            Dictionary<string, dynamic> updatedDict = new Dictionary<string, dynamic>();
            
            foreach (var key in dict.Keys)
            {
                if (dict[key].Contains("::"))
                {
                    var jsonFileAndKey = dict[key].ToString().Split(new string[] { "::" }, StringSplitOptions.None);

                    updatedDict.Add(key, GetRecord(jsonFileAndKey[0], jsonFileAndKey[1]));
                }
                else
                    updatedDict.Add(key, dict[key]);
            }
            return updatedDict;
        }
    }
}

public static class DictionaryExtensions
{
    public static string Get(this Dictionary<string, object> instance, string key)
    {
        try
        {
            var value = instance.ContainsKey(key) ? (string)instance[key] : "";
            return value;
        }
        catch(KeyNotFoundException)
        {
            Assert.Fail("The key provided could not be found. Please check the data file. Key: {0}", key);
            return null;
        }
    }
}


