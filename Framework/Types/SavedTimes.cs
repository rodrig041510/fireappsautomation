﻿using Framework.Config;

namespace Framework.Types
{
    public class SavedTimes
    {       /// <summary>
            /// The browser key
            /// </summary>
        private const string BrowserKey = "browser";

        /// <summary>
        /// The scenario
        /// </summary>
        private readonly string scenario;

        /// <summary>
        /// The browser name
        /// </summary>
        private readonly string browserName;

        /// <summary>
        /// The duration
        /// </summary>
        private double duration;

        /// <summary>
        /// Initializes a new instance of the <see cref="SavedTimes" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        public SavedTimes(string title)
        {
            scenario = title;
            browserName = Settings.BrowserType.ToString();
        }

        /// <summary>
        /// Gets the scenario.
        /// </summary>
        /// <value>
        /// The scenario.
        /// </value>
        public string Scenario
        {
            get { return scenario; }
        }

        /// <summary>
        /// Gets the name of the Driver.
        /// </summary>
        /// <value>
        /// The name of the Driver.
        /// </value>
        public string BrowserName
        {
            get { return browserName; }
        }

        /// <summary>
        /// Gets the duration.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public double Duration
        {
            get { return duration; }
        }

        /// <summary>
        /// Sets the duration.
        /// </summary>
        /// <param name="loadTime">The load time.</param>
        public void SetDuration(double loadTime)
        {
            duration = loadTime;
        }
    }
}
