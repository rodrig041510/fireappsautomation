﻿using OpenQA.Selenium;

namespace Framework.WebElements
{
    /// <summary>
    /// Implementation for JavaScript Alert interface.
    /// </summary>
    public class JavaScriptAlert
    {
        /// <summary>
        /// The web driver
        /// </summary>
        private readonly IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaScriptAlert"/> class.
        /// </summary>
        /// <param name="webDriver">The web driver.</param>
        public JavaScriptAlert(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Gets java script popup text
        /// </summary>
        public string JavaScriptText
        {
            get { return webDriver.SwitchTo().Alert().Text; }
        }

        /// <summary>
        /// Confirms the java script alert popup.
        /// </summary>
        public void ConfirmJavaScriptAlert()
        {
            webDriver.SwitchTo().Alert().Accept();
            webDriver.SwitchTo().DefaultContent();
        }

        /// <summary>
        /// Dismisses the java script alert popup.
        /// </summary>
        public void DismissJavaScriptAlert()
        {
            webDriver.SwitchTo().Alert().Dismiss();
            webDriver.SwitchTo().DefaultContent();
        }

        /// <summary>
        /// Method sends text to Java Script Alert
        /// </summary>
        /// <param name="text">Text to be sent</param>
        public void SendTextToJavaScript(string text)
        {
            webDriver.SwitchTo().Alert().SendKeys(text);
        }
    }
}
